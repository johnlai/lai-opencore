# Lai OpenCore

A EFI backup of my OpenCore configuration



## Configuration
**CPU:** Intel Core i7-7700K 4.2 GHz <br /> 
**Motherboard:** Gigabyte GA-H170N-WIFI Mini ITX <br /> 
**Video Card:** Sapphire Radeon RX 5700 XT 8 GB PULSE <br /> 
**Memory:** G.Skill Ripjaws 4 Series 16 GB (2 x 8 GB) DDR4-3000 <br /> 
**Storage:** Samsung 970 Evo 500 GB M.2-2280 NVME <br /> 
**Storage:** Seagate Barracuda 2 TB 3.5" 7200RPM <br /> 
**CPU Cooler:** Corsair H100i <br /> 
**Case:** Corsair Crystal 280X <br /> 
**Power Supply:** Corsair CX (2017) 650 W 80+ Bronze <br /> 
**Monitor:** LG 29WK600-W 29.0" 2560x1080 75 Hz <br /> 
